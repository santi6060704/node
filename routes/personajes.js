var express = require('express');
var router = express.Router();
var PersonajesController = require('../controllers/PersonajesController')

router.get("/verPersonajes",PersonajesController.verPersonajes)
router.get("/borrarPersonaje/:id",PersonajesController.borrarPersonaje)
router.get("/favorito",PersonajesController.favorito)

module.exports = router;
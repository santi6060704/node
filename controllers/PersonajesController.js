const Personajes = require('../models/Personajes')

exports.verPersonajes = (req, res) =>{
  Personajes.find().then(personaje =>{
    res.json({personaje})
  })
}

exports.favorito = (req, res) =>{
  const id = 6;
  Personajes.findOne({id:id}).then(personaje =>{
      res.json({personaje})
      .catch(err => {
        console.error(err);
        res.status(500).json({ msg: 'Ha habido un error, no ha podido encontrar el personaje favorito' });
    });
    })
}

exports.borrarPersonaje = (req, res) => {
  const id = req.params.id;
  Personajes.remove({ id: id })
      .catch(err => {
          console.error(err);
          res.status(500).json({ msg: 'Ha habido un error, no ha podido eliminar ningún personaje' });
      });
}